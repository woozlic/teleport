# Teleport

Teleport - WSGI Framework with instant movement of information and easy site development.

#HOW TO USE
1. Install requirements
```
pip install -r teleport_framework/requirements.txt
```
2. RUN
```
python -m example_website.run
```
OR
```
python example_website/run.py
```