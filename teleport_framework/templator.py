from jinja2 import Environment, FileSystemLoader


def render(template_name, folder='templates', **kwargs):
    """
    Overloaded jinja2 templator
    :param template_name: имя шаблона
    :param folder: папка с шаблонами
    :param kwargs: параметры для передачи в шаблон
    :return:
    """
    env = Environment(loader=FileSystemLoader(folder))
    template = env.get_template(template_name)
    return template.render(**kwargs)
