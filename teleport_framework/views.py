from teleport_framework.templator import render


def view(request: dict, template_name: str, **kwargs):
    print(request)
    return '200 OK', [bytes(render(template_name, **kwargs), 'utf-8')]
