from wsgiref.util import setup_testing_defaults

from teleport_framework.requests import GetRequest, PostRequest


def not_found(request):
    return '404 WHAT', [b'404 PAGE Not Found']


class Application:

    def __init__(self, routes, fronts, error_routes):
        self.routes = routes
        self.fronts = fronts
        self.error_routes = error_routes

    def __call__(self, environ, start_response):
        setup_testing_defaults(environ)
        path = self.router(environ['PATH_INFO'])
        if path in self.routes:
            view = self.routes[path]
        else:
            view = self.error_routes['404'] or not_found

        request = {}
        for front in self.fronts:
            front(request)

        method = environ['REQUEST_METHOD']
        request['method'] = method
        if method == 'GET':
            request_params = GetRequest().get_request_params(environ)
            request['request_params'] = request_params

        elif method == 'POST':
            data = PostRequest().get_request_params(environ)
            request['data'] = data

        code, body = view(request)
        start_response(code, [('Content-Type', 'text/html')])
        return body

    @staticmethod
    def router(route: str):
        if not route.endswith('/'):
            return route + '/'
        return route


class DebugApplication(Application):
    def __init__(self, routes, fronts, error_routes):
        self.application = Application(routes, fronts, error_routes)
        super().__init__(routes, fronts, error_routes)

    def __call__(self, environ, start_response):
        print(f'DEBUG: {environ}')
        return self.application(environ, start_response)


class FakeApplication(Application):
    def __init__(self, routes, fronts, error_routes):
        self.application = Application(routes, fronts, error_routes)
        super().__init__(routes, fronts, error_routes)

    def __call__(self, environ, start_response):
        start_response('200 OK', [('Content-Type', 'text/html')])
        return [b'Hello from FAKE']
