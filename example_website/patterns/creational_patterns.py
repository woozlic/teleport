import copy
import abc
import datetime


class CoursePrototype:

    def clone(self):
        return copy.deepcopy(self)


class Course(CoursePrototype):

    def __init__(self, name, category):
        self.name = name
        self.category = category
        self.category.courses.append(self)


class OfflineCourse(Course):
    pass


class OnlineCourse(Course):
    pass


class CourseFactory:
    types = {
        'offline': OfflineCourse,
        'online': OnlineCourse,
    }

    @classmethod
    def create(cls, type_, name, category):
        return cls.types[type_](name, category)


class Category:
    auto_id = 0

    def __init__(self, name, category):
        self.id = Category.auto_id
        Category.auto_id += 1
        self.name = name
        self.category = category
        self.courses = []

    def course_count(self):
        result = len(self.courses)
        if self.category:
            result += self.category.course_count()
        return result


class User:
    pass


class Teacher(User):
    pass


class Student(User):
    pass


class UserFactory:
    types = {
        'teacher': Teacher,
        'student': Student,
    }

    @classmethod
    def create(cls, type_):
        return cls.types[type_]()


class Engine:
    def __init__(self):
        self.students = []
        self.teachers = []
        self.categories = []
        self.courses = []
        self.routes = {}
        self.error_routes = {}

    @staticmethod
    def create_user(type_):
        return UserFactory().create(type_)

    @staticmethod
    def create_category(name, category=None):
        return Category(name, category)

    def find_category_by_id(self, id: int):
        for item in self.categories:
            if item.id == id:
                return item
        raise KeyError(f'Нет категории с id = {id}')

    @staticmethod
    def create_course(type_, name, category):
        return CourseFactory.create(type_, name, category)

    def get_course(self, name):
        for item in self.courses:
            if item.name == name:
                return item
        return None


class SingletonByName(type):

    def __init__(cls, name, bases, attrs, **kwargs):
        super().__init__(name, bases, attrs)
        cls.__instance = {}

    def __call__(cls, *args, **kwargs):
        if args:
            name = args[0]
        if kwargs:
            name = kwargs['name']

        if name in cls.__instance:
            return cls.__instance[name]
        else:
            cls.__instance[name] = super().__call__(*args, **kwargs)
            return cls.__instance[name]


class AbstractLogger(metaclass=SingletonByName):

    def __init__(self, name):
        self.name = name

    @abc.abstractmethod
    def log(self, text: str, delimiter: str):
        pass


class ConsoleLogger(AbstractLogger):

    def log(self, text, delimiter=' | '):
        now = datetime.datetime.now()
        message = delimiter.join((str(now), text))
        print(message)
