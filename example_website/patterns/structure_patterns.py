import time
from functools import wraps
from .creational_patterns import ConsoleLogger

debug_logger = ConsoleLogger('debug')


def url(routes: dict, url_path: str):
    def actual_wrapper(f):
        if f not in routes.values():
            copy_url_path = str(url_path)
            if not copy_url_path.endswith('/'):
                copy_url_path += '/'
            routes[copy_url_path] = f
            print('Added url', url_path, f.__name__)

        @wraps(f)
        def wrapper(*args, **kwargs):
            return f(*args, **kwargs)
        return wrapper
    return actual_wrapper


def debug(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        t1 = time.time()
        result = f(*args, **kwargs)
        t2 = time.time()
        ex_time = t2-t1
        debug_logger.log(f'{f.__name__} was executed in {str(round(ex_time, 2))} s.')
        return result
    return wrapper
