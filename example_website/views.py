from teleport_framework.views import view
from patterns.creational_patterns import Engine, ConsoleLogger
from patterns.structure_patterns import url, debug

site = Engine()
logger = ConsoleLogger('main')
routes = {}


error_routes = {
    '404': lambda request: view(request, '404.html'),
}


@url(routes, '/categories/')
@debug
def categories(request):
    return view(request, 'list-categories.html', objects_list=site.categories)


@url(routes, '/courses/')
@debug
def courses(request):
    try:
        category = site.find_category_by_id(int(request['request_params']['id']))
        category_courses = [c for c in site.courses if c.category.name == category.name]
        return view(request, 'courses.html', name=category.name, id=category.id, objects_list=category_courses)
    except KeyError:
        return view(request, 'courses.html', error='There are no courses yet.')


@url(routes, '/categories/create/')
@debug
def create_category(request):
    if request['method'] == 'POST':
        data = request['data']
        name = data['name']
        category_id = data.get('category_id')
        category = None
        if category_id:
            category = site.find_category_by_id(int(category_id))
        new_category = site.create_category(name, category)
        site.categories.append(new_category)
        return view(request, 'list-categories.html', objects_list=site.categories)
    else:
        return view(request, 'create-category.html', categories=site.categories)


@url(routes, '/courses/create/')
@debug
def create_course(request):
    if request['method'] == 'POST':
        data = request['data']
        name = data['name']
        category = None
        category_id = int(data['category_id'])
        if category_id != -1:
            category = site.find_category_by_id(int(category_id))
            course = site.create_course('online', name, category)
            site.courses.append(course)
            category_courses = [c for c in site.courses if c.category.name == category.name]
            return view(request, 'courses.html', objects_list=category_courses, name=category.name, id=category.id)
    else:
        try:
            category_id = int(request['request_params']['id'])
            category = site.find_category_by_id(int(category_id))
            return view(request, 'create-course.html', name=category.name, id=category.id)
        except KeyError:
            return view(request, 'create-course.html', error='No categories have been added yet')


# def copy_course(request):
#     request_params = request['request_params']
#     try:
#         name = request_params['name']
#         category_name = request_params['course']
#         category_id = int(request_params['id'])
#         old_course = site.get_course(name)
#         if old_course:
#             new_name = f'copy_{name}'
#             new_course = old_course.clone()
#             new_course.name = new_name
#             site.courses.append(new_course)
#             category_courses = [c for c in site.courses if c.category.name == new_course.category.name]
#             return view(request, 'courses.html', name=category_name, id=category_id, objects_list=category_courses)
#         else:
#             return view(request, 'courses.html', error='No such course')
#     except KeyError:
#         return view(request, 'courses.html', error='No courses have been added yet')


@url(routes, '/')
def news(request):
    return view(request, 'news.html')


@url(routes, '/about/')
def about(request):
    return view(request, 'about.html')


@url(routes, '/login/')
def login(request):
    return view(request, 'login.html')


@url(routes, '/contact/')
def contact(request):
    return view(request, 'contact.html')

