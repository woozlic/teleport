from wsgiref.simple_server import make_server
import os,sys
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
sys.path.append(os.path.dirname(__file__))
from teleport_framework.fronts import fronts
from teleport_framework.main import Application

from example_website.views import routes, error_routes

application = Application(routes, fronts, error_routes)


if __name__ == '__main__':
    with make_server('', 8000, application) as httpd:
        print("Serving on port 8000...")
        httpd.serve_forever()
